import os, os.path
import numpy as np
import glob
import imageio
import keras
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
#read in data  
images = [imageio.imread(path) for path in glob.glob('C:\\Users\\philip_grenfell\\OneDrive\\COMP5329 - Deep learning\\Assignment 2\\Assignment-2-Dataset-Round-1\\train-set\\*.png')]
images = np.asarray(images)

#visualise image
plt.imshow(images[0])

#read labels from file names and store as array
labels=[]
path = "C:\\Users\\philip_grenfell\\OneDrive\\COMP5329 - Deep learning\\Assignment 2\\Assignment-2-Dataset-Round-1\\train-set"
valid_images = [".jpg",".gif",".png",".tga"]
for f in os.listdir(path):
    labels.append(int(f[4:6])-1)
labels = np.asarray(labels)

#set parameters for model
data_input = images
label_input = labels
num_classes = 63
input_shape = (128, 128, 1)
epochs = 10
batch_size = 128

############# MODEL FOR KERAS #####################
#create test train split 
X_train, X_test, y_train, y_test = train_test_split(data_input, label_input, test_size=0.10, random_state=42, shuffle=True)

#convert to float and normalise
X_train = X_train.astype('float32')
X_train /=255
X_test = X_test.astype('float32') 
X_test=X_test/255

#reshape to be a 4d tensor 
X_train = X_train.reshape(X_train.shape[0], 128, 128, 1)
X_test = X_test.reshape(X_test.shape[0], 128, 128, 1)

#convert labels to cat for cross entropy loss
Y_train = keras.utils.to_categorical(y_train, 63)
Y_test = np_utils.to_categorical(y_test, 63)

#build model
model = Sequential()        
model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),activation='relu',input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
model.add(Conv2D(64, (5, 5), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(1000, activation='relu'))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adam(),
              metrics=['accuracy'])

#fit model
model.fit(X_train, Y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(X_test, Y_test))

#evaluate model
score = model.evaluate(X_test, Y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])



